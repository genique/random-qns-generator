#!/usr/bin/env python
"""
Very simple HTTP server in python.
Usage::
    ./dummy-web-server.py [<port>]
Send a GET request::
    curl http://localhost
Send a HEAD request::
    curl -I http://localhost
Send a POST request::
    curl -d "foo=bar&bin=baz" http://localhost
"""
from BaseHTTPServer import BaseHTTPRequestHandler, HTTPServer
import SocketServer
import os
import urlparse
from os import curdir, sep
from random import randint, shuffle

weblink = 'http://localhost:8888'
matches = []
categories = {} # keys of array
final_matches = []

# Helper Functions
def find_between( s, first, last ):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

def get_query_example():
    example = ''
    example += '?'
    for keys in categories:
        example += (keys + '=NUMBER OF QUESTIONS&')
    return example[:-1]

class S(BaseHTTPRequestHandler):
    def _set_headers(self):
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_GET(self):
        final_matches = []
        try:
            sendReply = False
            if self.path.endswith(".html"):
                mimetype='text/html'
                sendReply = True
            if self.path.endswith(".jpg"):
                mimetype='image/jpg'
                sendReply = True
            if self.path.endswith(".gif"):
                mimetype='image/gif'
                sendReply = True
            if self.path.endswith(".js"):
                mimetype='application/javascript'
                sendReply = True
            if self.path.endswith(".css"):
                mimetype='text/css'
                sendReply = True
            if self.path.endswith(".ico"):
                return
            if sendReply == True:
                #Open the static file requested and send it
                f = open(curdir + sep + self.path) 
                self.send_response(200)
                self.send_header('Content-type', mimetype)
                self.end_headers()
                self.wfile.write(f.read())
                f.close()
                return

        except IOError:
            self.send_error(404,'File Not Found: %s' % self.path)

        parsed_path = urlparse.urlparse(self.path)
        queries = {}
        if len(parsed_path.query.split("&")) > 1:
            query_components = dict(qc.split("=") for qc in parsed_path.query.split("&"))
            queries = query_components
        elif len(parsed_path.query.split("&")) > 0:
            query_components = parsed_path.query.split("=")
            if len(query_components) > 1:
                queries = { query_components[0] : query_components[1]}

        #If resource not found
        self._set_headers()

        query_example = get_query_example()
        self.wfile.write('<html><body style="text-align: center"><h1>Random Question Generator</h1><h6 style="max-width: 600px; margin: auto">Example Link: '+ weblink + get_query_example() + '</h6><br/>')
        counter = 0

        # Implement randomization based on queries {c: amt} and categories {c: [qns]}
        for cat_key in queries:
            qns_amt = queries[cat_key]
            if cat_key in categories:
                print "Generating " + str(qns_amt) + "/" +str(len(categories[cat_key]))+" question(s) for " + cat_key
                qns_amt = int(qns_amt)
                if (qns_amt > 0 and qns_amt <= len(categories[cat_key])):
                    available_qns_amt = len(categories[cat_key])
                    chosen_qns_set = []
                    for num in range(0,qns_amt):
                        rand_idx = randint(0, available_qns_amt - 1)
                        while rand_idx in chosen_qns_set:
                            rand_idx = randint(0, available_qns_amt - 1)
                        print '    [+] ' + categories[cat_key][rand_idx]
                        chosen_qns_set.append(rand_idx)
                        final_matches.append(categories[cat_key][rand_idx])
                else:
                    final_matches.extend(categories[cat_key])

        for i in range(0, len(final_matches)-1):
            pick = randint(i+1, len(final_matches)-1)
            final_matches[i], final_matches[pick] = final_matches[pick], final_matches[i]

        for imgname in final_matches:
            counter += 1
            self.wfile.write('<div class="question-div" style="max-width: 100%">Question '+ str(counter) + '<br/><img src=\"' + imgname + '\"></img></div>')
            self.wfile.write("<br/>")
        self.wfile.write("</body></html>")

    def do_HEAD(self):
        self._set_headers()
        
    def do_POST(self):
        # Doesn't do anything with posted data
        self._set_headers()
        self.wfile.write("<html><body><h1>POST!</h1></body></html>")

  
def run(server_class=HTTPServer, handler_class=S, port=80):
    weblink = 'http://localhost:'+ str(port)
    server_address = ('', port)
    httpd = server_class(server_address, handler_class)
    print 'Starting httpd server at http://localhost:' + str(port)
    httpd.serve_forever()


# Initial Program Run
if __name__ == "__main__":
    from sys import argv

    for root, dirnames, filenames in os.walk('./'):
        for filename in filenames:
            if filename.endswith(('.jpg', '.jpeg', '.gif', '.png')):
                matches.append(os.path.join(root, filename))
                category = find_between(root + '/', './questions/', '/')
                if category not in categories:
                    categories[category] = []
                categories[category].append(root+'/'+filename)


    print "Found " + str(len(matches)) + " Images"

    if len(argv) == 2:
        run(port=int(argv[1]))
    else:
        run()
