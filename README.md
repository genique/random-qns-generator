# Random Question Generator

### How it works

This generator will run a HTTP server and display questions randomly on the website.

```
Example on how to use:
Type the following on you MAC Terminal while you are in the directory of the python file
	python qns_randomizer.py 8888

Press ctrl+c to stop the server
Restart the server to refresh questions database

```


To view the website: Go to `http://localhost:8888`

To add more questions belonging to different categories, just create additional subfolders in the questions folder and drop your images inside (.jpg, .jpeg, .gif, .png)

** NOTE: Image name (.gif, .jpeg, .png, ...) should contain no spaces **

```
Example of the folder structure

> Project_Folder
	> README.md
	> qns_randomizer.py
	> questions
		> differentiations
			> img1.png
			> img2.gif

		> integrations
			> img1.gif

		> algebra
			> qns1.jpg

		> category1
			> qns.jpg

		> category2
			> qns.png

		......
		...
		..so on and so forth
```